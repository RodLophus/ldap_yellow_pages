<?php


/*
 * Aba DATA SEARCH
 */
function ldap_yp_admin_config() {
	module_load_include('inc', 'ldap_yp', 'ldap_yp_util');
	global $base_url;
	$help_url = $base_url . '/admin/help/ldap_yp';

	$enabled_ldap_queries = array();
	foreach(ldap_query_get_queries(NULL, 'enabled') as $qid => $query_parameters) {
		$enabled_ldap_queries[$qid] = $query_parameters->name;
	}

	$form['ldap_yp_base_query'] = array(
		'#type' => 'select',
		'#title' => t('Base LDAP query'),
		'#options' => $enabled_ldap_queries,
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_base_query'),
		'#description' => t('Query to use as base for searches. Must be defined on the <i>LDAP Query</i> module and enabled.')
	);

	$form['ldap_yp_search_attributes'] = array(
		'#type' => 'textfield',
		'#title' => t('Search attributes (comma-separated list)'),
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_search_attributes'),
		'#description' => t('List of LDAP attributes used to search the keyword value against.')
	);

	$form['ldap_yp_url_search_attribute'] = array(
		'#type' => 'textfield',
		'#title' => t('Search attribute for contextual (URL) queries'),
		'#default_value' => ldap_yp_var_get('ldap_yp_url_search_attribute'),
		'#description' => t('LDAP attribute used for contextual searches. The keyword passed as parameter on URL "<i>result_block_url</i>/query/keyword"<br/>will be matched against this attribute and the results will be shown on the initial results page.')
	);

	$form['ldap_yp_sorting_attribute'] = array(
		'#type' => 'textfield',
		'#title' => t('Sorting attribute'),
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_sorting_attribute'),
		'#description' => t('LDAP attribute (among the ones defined by the Base Query) used to sort the results list.')
	);

	$form['ldap_yp_null_search'] = array(
		'#type' => 'fieldset',
		'#title' => t('Null-search results'),
		'#description' => t('What to show on the results block when no search parameters have been specified.')
	);

	$form['ldap_yp_null_search']['ldap_yp_null_behavior'] = array(
		'#type' => 'select',
		'#title' => t('Null-search behaviour'),
		'#options' => array(
			LDAP_YP_NB_PAGE  => t('Static page'),
			LDAP_YP_NB_QUERY => t('Static LDAP query result')
		),
		'#default_value' => ldap_yp_var_get('ldap_yp_null_behavior')
	);

	$form['ldap_yp_null_search']['ldap_yp_null_page'] = array(
		'#type' => 'textarea',
		'#title' => t('Null-search static page contents'),
		'#default_value' => ldap_yp_var_get('ldap_yp_null_page'),
		'#description' => t('Static page to be shown when no search parameters have been specified.'),
		'#states' => array(
			'visible' => array(
				':input[name="ldap_yp_null_behavior"]' => array('value' => LDAP_YP_NB_PAGE),
			)
		)
	);

	$form['ldap_yp_null_search']['ldap_yp_null_filter'] = array(
		'#type' => 'textfield',
		'#title' => t('Null-search LDAP filter '),
		'#default_value' => ldap_yp_var_get('ldap_yp_null_filter'),
		'#description' => t('LDAP filter used when no search parameters have been specified. Leave blank to display all entries.'),
		'#states' => array(
			'visible' => array(
				':input[name="ldap_yp_null_behavior"]' => array('value' => LDAP_YP_NB_QUERY),
			)
		)
	);

	$form['ldap_yp_result_limit'] = array(
		'#type' => 'textfield',
		'#title' => t('Result size limit'),
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_result_limit'),
		'#description' => t('Maximum number of result values to be displayed. Use "0" for "no limit".')
	);

	$form['ldap_yp_search_menu'] = array(
		'#type' => 'textarea',
		'#title' => t('Search parameters menu definition'),
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_search_menu'),
		'#description' => t('Description of the parametric search menu, according to the syntax: &lt;level&gt;,&lt;group&gt;:&lt;menu_name&gt;:&lt;menu_item&gt;,&lt;ldap_filter&gt;:&lt;menu_item&gt;,&lt;ldap_filter&gt;:...<br/>' . 
			'Please see the <a href="@help_page" target="_blank">module\'s help section</a> for examples and syntax details.',
			array('@help_page' => $help_url . '#search_menu'))
	);

	$form['ldap_yp_debug'] = array(
		'#type' => 'fieldset',
		'#title' => t('Debug')
	);

	$form['ldap_yp_search_class'] = array(
		'#type' => 'textfield',
		'#title' => t('Class to add to the search block'),
		'#default_value' => ldap_yp_var_get('ldap_yp_search_class'),
		'#description' => t('This CSS class will be added to the search block\'s containter.')
	);


	$form['ldap_yp_debug']['ldap_yp_show_filter'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show LDAP search filter on results block'),
		'#default_value' => ldap_yp_var_get('ldap_yp_show_filter'),
		'#description' => t('Show the (final) calculated LDAP filter on the top of the results block.')
	);

	$form['ldap_yp_debug']['ldap_yp_clear_cache'] = array(
		'#type' => 'checkbox',
		'#title' => t('Clear cache on configuration save'),
		'#description' => t('To clear the module\'s cache, enable this option and click the "Save" button bellow.')
	);

	$form['#validate'][] = 'ldap_yp_admin_config_search_validate';
	$form['#submit'][] = 'ldap_yp_admin_config_search_submit';

	return system_settings_form($form);
}


/*
 * Aba DATA PRESENTATION
 */
function ldap_yp_admin_presentation() {
	module_load_include('inc', 'ldap_yp', 'ldap_yp_util');
	global $base_url;
	$help_url = $base_url . '/admin/help/ldap_yp';

	// Templates
	$form['ldap_yp_results_template'] = array(
		'#type' => 'fieldset',
		'#title' => t('Results rendering template')
	);

	// Templates - Head
	$form['ldap_yp_results_template']['ldap_yp_results_head'] = array(
		'#type' => 'textarea',
		'#title' => t('Head'),
		'#default_value' => ldap_yp_var_get('ldap_yp_results_head'),
		'#description' => t('Top portion of the result page (evaluated only once). This field takes HTML commands and various substitution patterns.<br/>' .
			'Please refer to the <a href="@help_page" target="_blank">module\'s help section</a> for examples and syntax details.',
			array('@help_page' => $help_url . '#result_templates'))
	);

	// Templates - Body
	$form['ldap_yp_results_template']['ldap_yp_results_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Body'),
		'#required' => TRUE,
		'#default_value' => ldap_yp_var_get('ldap_yp_results_body'),
		'#description' => t('The portion of the result page wich contains the actual results (shown once per result found). This field takes HTML commands and complex substitution patterns.<br/>' . 
			'Please refer to the <a href="@help_page" target="_blank">module\'s help section</a> for examples and syntax details.',
			array('@help_page' => $help_url . '#result_templates'))
	);

	// Templates - Tail
	$form['ldap_yp_results_template']['ldap_yp_results_tail'] = array(
		'#type' => 'textarea',
		'#title' => t('Tail'),
		'#default_value' => ldap_yp_var_get('ldap_yp_results_tail'),
		'#description' => t('Botton portion of the result page (evaluated only once). This field takes HTML commands and various substitution patterns.<br/>' .
			'Please refer to the <a href="@help_page" target="_blank">module\'s help section</a> for examples and syntax details.',
			array('@help_page' => $help_url . '#result_templates'))
	);

	// Templates - Empty
	$form['ldap_yp_results_template']['ldap_yp_results_empty'] = array(
		'#type' => 'textarea',
		'#title' => t('Empty result content'),
		'#default_value' => ldap_yp_var_get('ldap_yp_results_empty'),
				'#description' => t('This template will be displayed (instead of the head+body+tail template above) when the search returns no results. This field takes HTML commands.')
	);

	// Table tags
	$form['ldap_yp_table_tags'] = array(
		'#type' => 'fieldset',
		'#title' => t('Table formating tags'),
		'#description' => t('These parameters controls the generation of the $body{col}$ and $body{row}$ substitution patterns, which can be used on the "Body" template (above) to generate class names and ID tags to make the search results in a tabular format.'),
	);

	// Table tags - Cols
	$form['ldap_yp_table_tags']['ldap_yp_table_cols'] = array(
		'#type' => 'select',
		'#title' => t('Columns'),
		'#options' => array_merge(array('1', '2', '3', '4', '5')),
		'#default_value' => ldap_yp_var_get('ldap_yp_table_cols')
	);

	// Table tags - Rows
	$form['ldap_yp_table_tags']['ldap_yp_table_rows'] = array(
		'#type' => 'select',
		'#title' => t('Rows'),
		'#options' => array_merge(array('1', '2', '3', '4', '5')),
		'#default_value' => ldap_yp_var_get('ldap_yp_table_rows')
	);

	$form['ldap_yp_landing_page'] = array(
		'#type' => 'checkbox',
		'#title' => t('Generate a blank landing page for the results block.'),
		'#default_value' => ldap_yp_var_get('ldap_yp_landing_page'),
		'#description' => t('If allowed, this option will generate a blank page on the URL attributed to the results block, to prevent Drupal from generating a "page not found" error if there is no main content on that location.<br>Caution: enable this option only if you have configured the results block to appear in only one location (via Structure -> Blocks on the site\'s configuration) and if this location is not going to have a normal "main content".<br>Note: the site will show the "page not found" error once (and only once) when accessing the results block\'s location when this option is enabled or when the results block\'s location have been changed.'),
	);

	$form['#submit'][] = 'ldap_yp_admin_config_presentation_submit';

	return system_settings_form($form);
}


function ldap_yp_admin_config_search_validate($form, $form_state) {
	$values = $form_state['values'];
	$search_class = $values['ldap_yp_search_class'];
	$null_filter = $values['ldap_yp_null_filter'];
	$search_attributes = $values['ldap_yp_search_attributes'];
	$search_menu_items = explode("\n", $values['ldap_yp_search_menu']);
	
	$line_number = 1;
	foreach($search_menu_items as $menu_item) {
		$menu_item = trim($menu_item);
		if(! preg_match('/^(0|[0-9]+,[^:,\(\)]+):[^:,\(\)]+(:[^:,\(\)]+(,\([a-z0-9()&|*<>=~\s]+\))?)+$/i', $menu_item)) {
			form_set_error('ldap_yp_search_menu', t('Invalid search menu item definition at line %n.', array('%n' => $line_number)));
			break;
		}
		$line_number ++;
	}

	if(preg_match('/[\s_]/', $search_class))
		form_set_error('ldap_yp_search_class', t('Space and underline ("_") are not allowed in class name.'));

	if(! preg_match('/^\([a-z0-9()&|\*<>=~\s]+\)$/i', $null_filter))
		form_set_error('ldap_yp_null_filter', t('Invalid null-search LDAP filter.'));

	if(! preg_match('/^[a-z0-9]+(\s*,\s*[a-z0-9]+)*$/i', $search_attributes))
		form_set_error('ldap_yp_search_attributes', t('Invalid search attributes list.'));
}


/*
 * Hook Form Submit
 */
function ldap_yp_admin_config_search_submit($form, $form_state) {
	$clear_cache = $form_state['values']['ldap_yp_clear_cache'];
	$search_menu_items = explode("\n", $form_state['values']['ldap_yp_search_menu']);

	$search_options = array();

	//Estrutura de $search_menu_items:
	// level_token:title:option_token:option_token...
	// Onde: level_token  = level,group
	//       option_token = menu_option,ldap_filter
	//
	// Estrutura de $search_options:
	// [level] => array(
	//   'group' => array(
	//     'title' => 'title',
	//       'options' => array(
	//         'menu_option' => 'ldap_filter',
	//         'menu_option' => 'ldap_filter',...
	//       )
	//     )
	//   );
	foreach($search_menu_items as $menu_item) {
		$tokens = explode(':', $menu_item);

		$level_token = explode(',', $tokens[0]);
		$level = intval($level_token[0]);
		$group = ($level === 0) ? 'Main' : trim($level_token[1]);

		$search_options[$level][$group]['title'] = $tokens[1];

		for($i = 2; $i < count($tokens); $i++) {
			$option_token = explode(',', $tokens[$i]);
			$option_name   = trim($option_token[0]);
			$option_filter = isset($option_token[1]) ? trim($option_token[1]) : '';

			$search_options[$level][$group]['options'][$option_name] = $option_filter;
		}
	}

	variable_set('ldap_yp_search_options', $search_options);

	if($clear_cache) {
		cache_clear_all('*', 'cache_ldap_yp', TRUE);
		drupal_set_message(t('The cache for substitution patterns have been cleared.'));
	}
}


/*
 * Hook Form Submit - aba DATA PRESENTATION
 */
function ldap_yp_admin_config_presentation_submit($form, $form_state) {
	$landing_page = $form_state['values']['ldap_yp_landing_page'];

	if($landing_page)
		variable_set('ldap_yp_results_path', '__update_me__');
	else {
		variable_set('ldap_yp_results_path', '');
		menu_rebuild();
	}
}

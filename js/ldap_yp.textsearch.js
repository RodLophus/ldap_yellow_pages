(function($) {
	$(function() {
		$('input.delayed-search-submit').once('event', function() {
			var ignoredKeyCode = [
				16, // shift
				17, // ctrl
				18, // alt
				20, // caps lock
				33, // page up
				34, // page down
				35, // end
				36, // home
				38, // up arrow
				40, // down arrow
				 9, // tab
				27  // esc
			];
			var $self = $(this);
			var timeout = null;
			var delay = $self.data('delay');
			var triggerEvent = $self.data('event');
			if (! delay) {
				delay = 1000;
			}
			if (! triggerEvent) {
				triggerEvent = 'finished_input';
			}
			$self.bind('keyup keydown', function(e) {
				if ($.inArray(e.keyCode, ignoredKeyCode) != -1) {
					return false;
				}
				if (e.keyCode == 13) { // enter
					e.preventDefault();
					$self.trigger(triggerEvent);
					return true;
				}
				clearTimeout(timeout);
				timeout = setTimeout(function() {
					$self.trigger(triggerEvent);
				}, delay);
			});
		});
	});
})(jQuery);

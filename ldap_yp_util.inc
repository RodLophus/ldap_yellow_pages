<?php

// Para o campo ldap_yp_null_behavior
define('LDAP_YP_NB_PAGE',  '1');
define('LDAP_YP_NB_QUERY', '2');

/*
 * [INTERNAL] Wrapper arround variable_get(), to define default values only once
 * and in only one place
 */
function ldap_yp_var_get($var) {
	$defaults = array(
		'ldap_yp_base_query' => NULL,
		'ldap_yp_search_attributes' => 'cn',
		'ldap_yp_url_search_attribute' => '',
		'ldap_yp_sorting_attribute' => 'cn',
		'ldap_yp_null_behavior' => LDAP_YP_NB_PAGE,
		'ldap_yp_null_page' => '',
		'ldap_yp_null_filter' => '(objectClass=*)',
		'ldap_yp_result_limit' => 50,
		'ldap_yp_search_menu' => '',
		'ldap_yp_search_class' => '',
		'ldap_yp_search_options' => array(),
		'ldap_yp_results_head' => '',
		'ldap_yp_results_body' => '',
		'ldap_yp_results_tail' => '',
		'ldap_yp_results_empty' => '',
		'ldap_yp_table_cols' => 1,
		'ldap_yp_table_rows' => 1,
		'ldap_yp_landing_page' => 0,
		'ldap_yp_show_filter' => 0,
		'ldap_yp_clear_cache' => 0,
		'ldap_yp_results_path' => ''
	);

	if($var == '_list_')
		return array_keys($defaults);
	else
		return variable_get($var, $defaults{$var});
}


/*
 * [INTERNAL] Retorna o filtro LDAP de busca parametrica conforme
 *            configuracao do menu
 */
function ldap_yp_filter($level, $group, $key) {
	module_load_include('inc', 'ldap_yp', 'ldap_yp_util');
	$config = ldap_yp_var_get('ldap_yp_search_options');

	if(isset($config[$level][$group]))
		$data = $config[$level][$group]['options'];
	else
		// Tupla nivel+grupo+chave invalida: retorna um filtro com resultado nulo
		return '(objectClass=INVALID_FILTER)';

	$filters = array();
	if(($key == NULL) || ($key == 'ALL'))
		// Concatena todos os filtros definidos no nivel atual
		foreach(array_keys($data) as $child)
			array_push($filters, ldap_yp_filter($level, $group, $child));
	else
		if(empty($data[$key]))
			// Elemento nao tem filtro: concatena os filtros do nivel seguinte
			array_push($filters, ldap_yp_filter($level + 1, $key, 'ALL'));
		else
			// Retorna o filtro
			array_push($filters, $data[$key]);
	
	return ldap_yp_glue_filters($filters, 'or');
}


/*
 * [INTERNAL] Retorna um "OR" ou "AND" dos filtros passados como parametro
 */
function ldap_yp_glue_filters($filters, $operator) {
	$op = array('and' => '&', 'or' => '|');
	$filter = implode($filters);
	if(count($filters) > 1)
		$filter = '(' . $op[$operator] . $filter . ')';

	return $filter;
}


/*
 * [INTERNAL] Executa a pesquisa principal no LDAP e retorna os resultados ordenados
 */
function ldap_yp_process_search_query($filter) {
	$base_query = ldap_yp_var_get('ldap_yp_base_query');
	$size_limit = ldap_yp_var_get('ldap_yp_result_limit');

	$result = array();

	if($query = ldap_query_get_queries($base_query, 'enabled', TRUE)) {
		$query->filter = $result['filter'] = '(&' . $query->filter . $filter . ')';
		$query->sizelimit = 0;
		if(empty($result['data'] = $query->query()))
			return NULL;
	}

	if(! $result['data']['count'])
		return NULL;

	// Remove ['count'] para poder ordenar o array de resultados
	$result_count = $result['data']['count'];
	unset($result['data']['count']);

	// Ordena os resultados pelo criterio especificado
	usort($result['data'], function($a, $b) { 
		$criteria = strtolower(ldap_yp_var_get('ldap_yp_sorting_attribute'));
		return empty($a[$criteria][0]) || empty($b[$criteria][0]) ? 0 : 
			strnatcmp($a[$criteria][0], $b[$criteria][0]); 
	});

	if($size_limit && $result_count > $size_limit) {
		// Quantidade de resultados maior que o limite: trunca o resultado
		$result['data'] = array_slice($result['data'], 0, $size_limit);
		$result['truncated'] = TRUE;
	} else
		$result['truncated'] = FALSE;

	return $result;
}


/*
 * [INTERNAL] Procura $input_parameter=$key na query $query_name e retorna $output_parameter
 */
function ldap_yp_get_ldap_value($key, $input_parameter, $query_name, $output_parameter) {
	$result = '';
	$cid = strtolower('query_result:' . $query_name . ':' . $output_parameter . ':by_' . $input_parameter);

	if($cache = cache_get($cid, 'cache_ldap_yp'))
		$data = $cache->data;

	if(empty($data))
		if($query = ldap_query_get_queries($query_name, 'enabled', TRUE, TRUE))
			if($result = $query->query()) {
				for($i = 0; $i < $result['count']; $i++)
					if(isset($result[$i][$input_parameter][0]) && isset($result[$i][$output_parameter][0]))
						$data[$result[$i][$input_parameter][0]] = $result[$i][$output_parameter][0];
				cache_set($cid, $data, 'cache_ldap_yp', REQUEST_TIME + 60*60*24);
			}

	if(isset($data[$key]))
		$result = $data[$key];

	return $result;
}


/*
 * [INTERNAL] Substitui a ocorrencia de comandos do tipo $command{parameter}$
 *            pelos respespectivos resultados
 */
function ldap_yp_evaluate_simple_commands($line, $cmd, $params) {
	$result = '';
	
	$matches = array();
	while(preg_match('/\$([a-z0-9]+){([a-z0-9]+)}\$/i', $line, $matches, PREG_OFFSET_CAPTURE)) {
		$command   = strtolower($matches[1][0]);
		$parameter = strtolower($matches[2][0]);
		$offset = $matches[0][1];
		$length = strlen($matches[0][0]);
		if($command == $cmd)
			if(isset($params[$parameter]))
				$replace_str = $params[$parameter];
			else
				$replace_str = '??' . $parameter . '??';
		else
			$replace_str = '??' . $command . '??';
		$line = substr_replace($line, $replace_str, $offset, $length);
	} // while
	return $line;
}
